
let setLinksSpaces = (filter, data, tag) => {
    let spacediv = document.getElementById('links-container')
    let space_content = ''
    for(let i = 0 ; i < data.length ; i++){
        if (
          (filter === null || filter === '' || data[i].name.toLowerCase().includes(filter.toLowerCase()) || data[i].url.toLowerCase().includes(filter.toLowerCase())) &&
          (tag === null || data[i]['tags'].includes(tag))
        ) {
            let override_style = ''
            if (data[i]['bg_color'] != null) {
              override_style += 'background-color: ' + data[i]['bg_color']
            }
            space_content += `
                <div class="link" style="${override_style}">
                    <a href="${data[i].url}" class="link-picture">
                        <i class="${data[i].icon} fa-4x"></i>
                        ${data[i].name}
                        <div class="link-tags">#${data[i].tags.join(' #')}</div>
                    </a>
                </div>
            `
        }
    }
    spacediv.innerHTML = space_content
}

let selectTag = function (tag_name) {
  if (current_tag_selected === tag_name) {
    current_tag_selected = null
  } else {
    current_tag_selected = tag_name
  }

  for (let i = 0 ; i < tags_list.length ; i++) {
    if (tags_list[i]['name'] === current_tag_selected) {
      document.getElementById(tags_list[i]['id']).classList.add('tag-button-selected')
    } else {
      document.getElementById(tags_list[i]['id']).classList.remove('tag-button-selected')
    }
  }

  setLinksSpaces(document.getElementById('link-search-input').value, links_data, current_tag_selected)
}

let getLinksData = function () {
    fetch('data.json')
        .then(function(response) {
            response.json().then(function(data) {
                links_data = data.data
                document.title = data.page_title
                current_tag_selected = data.default_tag_selected

                for(let i = 0 ; i < links_data.length ; i++){
                  for(let j = 0 ; j < links_data[i]['tags'].length ; j++){
                    let tag_already_set = false
                    for(let k = 0 ; k < tags_list.length ; k++) {
                      if (tags_list[k]['name'] === links_data[i]['tags'][j]) {
                        tag_already_set = true
                      }
                    }

                    if (!tag_already_set) {
                      tags_list.push({
                          name: links_data[i]['tags'][j]
                      })
                    }
                  }
                }

                let tags_list_html = ''
                for(let i = 0 ; i < tags_list.length ; i++){
                  tags_list[i]['id'] = 'tag_' + i.toString()

                  let button_classes = 'tag-button'
                  if (current_tag_selected === tags_list[i]['name']) {
                    button_classes += ' tag-button-selected'
                  }
                  tags_list_html += `
                    <button type="button" id="${tags_list[i]['id']}" class="${button_classes}" onclick="selectTag('${tags_list[i]['name']}')">
                      ${tags_list[i]['name']}
                    </button>
                  `
                }
                document.getElementById('tags-list').innerHTML = tags_list_html

                document.getElementById('footer-content').innerHTML = data.footer_content

                setLinksSpaces(null, links_data, current_tag_selected)
            })
        })
}

let links_data = []
let tags_list = []
let current_tag_selected = null

getLinksData()

document.getElementById('link-search-input')
    .addEventListener("keyup", function(event){
        setLinksSpaces(event.target.value, links_data, current_tag_selected)
    })
