# Links page

Simple static page with links defined by json config file.

## Usage

### Basic usage

Set `public` folder as webserver docroute and create `data.json` like the following example.

```json
{
    "page_title": "My links",
    "default_tag_selected": "tag1",
    "footer_content": "<div>html content of my footer</div>",
    "data": [
        {
            "name": "LinkName",
            "url": "http://url/to/link",
            "icon": "fas fa-cogs",
            "tags": ["tag1", "tag2"]
        },
        {
            "name": "OtherLink",
            "url": "http://url/to/other/link",
            "icon": "fas fa-running",
            "tags": [],
            "bg_color": "#AD163F"
        }
    ]
}
```

### With docker

```bash
docker run -p "8080:80" -v "$PWD/data.json:/usr/share/nginx/html/data.json" registry.gitlab.com/barlebenx/html-links-page:latest
```
